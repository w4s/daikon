jQuery(document).ready(function ($) {
	// Open hidden menu
	$('.hamburger').click(function () {
		$(this).toggleClass('is-active');
		$('#hidden-menu').toggle('slow');
	});
	// Owl-Carousel
	$('.owl-home').owlCarousel({
		loop: true,
		nav: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		autoplay: true,
		autoplayTimeout: 10000,
		dotsContainer: '#customDots',
		items: 1
	});
	//Mobile menu
	$('#open-mobile-menu').click(function () {
		$('.mobile-menu').toggle('fast');
		return false;
	});
	$('.mobile-menu .menu-item-has-children > a').click(function () {
		$(this).next().toggle('fast');
		return false;
	});
	//Add transparent text
	$('.page-title').each(function (index) {
		var lastWord = $(this).text().split(' ');
		$(this).append('<span class="transparent-text">' + lastWord[lastWord.length - 1] + '</span>');
	});
	$('.section-title').each(function (index) {
		var lastWord = $(this).text().split(' ');
		$(this).append('<span class="transparent-text">' + lastWord[lastWord.length - 1] + '</span>');
	});
	$('.slide-content h2').each(function (index) {
		var lastWord = $(this).text().split(' ');
		$(this).after('<span class="transparent-text">' + lastWord[lastWord.length - 1] + '</span>');
	});
	//Testimonial form
	$('#open-add-testimonial').click(function () {
		$('#add-testimonial').toggle('fast');
		return false;
	});
	$('#close-add-testimonial').click(function () {
		$(this).parent().toggle('fast');
	});

	//Open pop-up post
	$('a.ajax-post').off('click').on('click', function (e) {

		$.fancybox.open($('.hidden-content'), {
			touch: false
		});

		/** Prevent Default Behaviour */
		e.preventDefault();

		/** Get Post ID */
		var post_id = $(this).attr('id');

		/** Ajax Call */
		$.ajax({

			cache: false,
			timeout: 8000,
			url: php_array.admin_ajax,
			type: "POST",
			data: ({ action: 'theme_post_example', id: post_id }),

			beforeSend: function () {
				$('#ajax-response').html('<div class="preloader"><div class="loader"></div></div>');
			},

			success: function (data, textStatus, jqXHR) {

				var $ajax_response = $(data);
				$('#ajax-response').html($ajax_response);

			},

			error: function (jqXHR, textStatus, errorThrown) {
				console.log('The following error occured: ' + textStatus, errorThrown);
			},

			complete: function (jqXHR, textStatus) {
			}

		});

	});

	//Close header-block mobile-app
	if ( 	$.cookie('hide') ) {
		$('.mobile-app').hide();
	}
	$('.mobile-app__close').click(function () {
		$('.mobile-app').hide();
		$.cookie('hide', true);
		return false;
	});

	//Yandex map
	if (document.getElementById('ya-map')) {
		ymaps.ready(init);
	}
	function init() {
		var myMap = new ymaps.Map("ya-map", {
			center: [53.198316, 45.010204],
			zoom: 15
		});

		var myPlacemark = new ymaps.Placemark([53.198316, 45.010204], {
			hintContent: 'Дайкон - суши бар',
			balloonContent: 'Плеханова, 14 (напротив Цирка)'
		}, {
				iconColor: 'red'
			});

		myMap.geoObjects.add(myPlacemark);
	}
});